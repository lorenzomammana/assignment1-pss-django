## Test runner

```
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor docker \
  --description "assignment1-pss-dind" \
  --docker-image "docker:stable" \
  --docker-privileged \
  --docker-network-mode bridge
```

## Deployment runner

```
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor shell \
  --description "assignment1-pss-shell" \
```