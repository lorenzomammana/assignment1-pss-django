# Assignment1 ProSviSo 2018

----

The source code of the project is available here: https://gitlab.com/lorenzomammana/assignment1-pss-django/

The live application is available here: http://90.147.189.102/

----

## Description

This first assignment is a simple application that allows registered users to create polls and vote them.
The application backend is written in Python using the MVC framework Django, while the frontend is mainly written
in HTML5 with a little of css and javascript.

Data is stored in a PostgreSQL database.

