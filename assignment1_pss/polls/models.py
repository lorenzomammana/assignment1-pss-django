import datetime

from django.db import models
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin
from django.conf import settings


# Model for the questions
class Question(ExportModelOperationsMixin('question'), models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    # The question is created by a user
    created_by = models.ForeignKey(
      settings.AUTH_USER_MODEL,
      on_delete=models.CASCADE
    )

    def __str__(self):
        return self.question_text

    # Check if the question was published within 24 hours
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now


# Model for the choices
class Choice(ExportModelOperationsMixin('choice'), models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

