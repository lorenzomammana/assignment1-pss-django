from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from braces.views import LoginRequiredMixin
from .models import Choice, Question
from django.utils import timezone


# Shows all questions available
class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')


# LoginRequiredMixin blocks access to views to unregistered users
# Shows all the choices available for a specific question
class DetailView(LoginRequiredMixin, generic.DetailView):
    login_url = '/polls'
    model = Question
    template_name = 'polls/detail.html'


# Shows the voting results for a specific question
class ResultsView(LoginRequiredMixin, generic.DetailView):
    login_url = '/polls'
    model = Question
    template_name = 'polls/results.html'


# Allows registered users to create new questions
class QuestionView(LoginRequiredMixin, generic.TemplateView):
    login_url = '/polls'
    template_name = 'polls/newquestion.html'

    def post(self, request, *args, **kwargs):
        return create_new_question(request)


# Add a new question and its relative choices into db
def create_new_question(request):
    question = request.POST['question']
    answers = request.POST['answer']

    if question != '' and answers != '':
        q = Question(question_text=question,
                     pub_date=timezone.now(),
                     created_by=request.user)

        q.save()

        for answer in reversed(answers.split(',')):
            q.choice_set.create(choice_text=answer,
                                votes=0)

        return HttpResponseRedirect(reverse('polls:index'))
    else:
        return render(request, 'polls/newquestion.html', {
            'error_message': "La domanda non può essere vuota",
        })


# Vote for the question specified by question_id
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
