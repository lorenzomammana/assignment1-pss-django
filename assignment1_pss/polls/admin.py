from django.contrib import admin

from .models import Question
from .models import Choice

# Allow admin to create new questions and choices from the web interface

admin.site.register(Question)
admin.site.register(Choice)
