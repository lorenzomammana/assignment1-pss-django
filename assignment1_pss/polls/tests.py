import datetime

from django.test import TestCase
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from .models import Question


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)


class SeleniumTest(TestCase):

    def setUp(self):
        self.firefox = webdriver.Remote(
            command_executor='http://selenium_hub:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX
        )
        self.firefox.implicitly_wait(10)

    def tearDown(self):
        self.firefox.quit()

    def test_suite(self):
        """
        signup a new user and then use it to login and create a new question
        """
        self.signup()
        self.login_newquestion()

    def signup(self):
        self.firefox.get('http://djangoapp:8000/users/signup')

        username = self.firefox.find_element_by_id('id_username')
        email = self.firefox.find_element_by_id('id_email')
        pwd1 = self.firefox.find_element_by_id('id_password1')
        pwd2 = self.firefox.find_element_by_id('id_password2')
        submit = self.firefox.find_element_by_id('id_submit')

        username.send_keys('testuser')
        email.send_keys('test@test.it')
        pwd1.send_keys('pizzapasta')
        pwd2.send_keys('pizzapasta')
        submit.click()

        assert 'Login' in self.firefox.page_source

    def login_newquestion(self):
        self.firefox.get('http://djangoapp:8000/users/login')
        username = self.firefox.find_element_by_id('id_username')
        pwd = self.firefox.find_element_by_id('id_password')
        submit = self.firefox.find_element_by_tag_name('button')
        username.send_keys('testuser')
        pwd.send_keys('pizzapasta')
        submit.click()

        question_link = self.firefox.find_element_by_id('id_newquestion')
        question_link.click()

        question = self.firefox.find_element_by_id('id_question')
        answers = self.firefox.find_element_by_id('id_answers')
        submit = self.firefox.find_element_by_id('id_submit')

        question.send_keys('Funzionerà questa domanda?')
        answers.send_keys('si,no')
        submit.click()

        assert 'Domande disponibili' in self.firefox.page_source

