from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

urlpatterns = [
    path('users/', include('django.contrib.auth.urls')),
    path('users/', include('users.urls')),
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
    # Include prometheus metrics url
    path('', include('django_prometheus.urls')),
    # This will redirect / to /polls
    path('', RedirectView.as_view(pattern_name='polls:index')),
]
