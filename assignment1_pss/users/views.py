from django.urls import reverse_lazy
from django.views import generic
from .forms import CustomUserCreationForm


# Allows the registration of new users
class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'users/signup.html'
