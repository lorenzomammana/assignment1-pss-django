from django.contrib.auth.models import AbstractUser
from django_prometheus.models import ExportModelOperationsMixin


# Extends the basic django user authentication
class CustomUser(ExportModelOperationsMixin('user'), AbstractUser):

    def __str__(self):
        return self.get_username()
